from api.models import Customer, Restaurant, Menu, SubMenu, Order, MenuItem
from django.contrib.auth.models import User
from rest_framework import viewsets, permissions
from .serializers import CustomerSerializer, RestaurantSerializer, UserSerializer , MenuSerializer, SubMenuSerializer, OrderSerializer, MenuItemSerializer
from rest_framework.permissions import IsAuthenticated

"""
ViewSet
"""

class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = UserSerializer

class CustomerViewSet(viewsets.ModelViewSet):
	queryset = Customer.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = CustomerSerializer

class RestaurantViewSet(viewsets.ModelViewSet):
	queryset = Restaurant.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = RestaurantSerializer

class MenuViewSet(viewsets.ModelViewSet):
	queryset = Menu.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = MenuSerializer

class SubMenuViewSet(viewsets.ModelViewSet):
	queryset = SubMenu.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = SubMenuSerializer

class OrderViewSet(viewsets.ModelViewSet):
	queryset = Order.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = OrderSerializer

class MenuItemViewSet(viewsets.ModelViewSet):
	queryset = MenuItem.objects.all()
	permission_classes = (IsAuthenticated,)
	serializer_class = MenuItemSerializer