from django.contrib import admin
from .models import Customer, Restaurant, Menu, SubMenu, Order, MenuItem

# Register your models here.
admin.site.register(Customer)
admin.site.register(Restaurant)
admin.site.register(Menu)
admin.site.register(SubMenu)
admin.site.register(Order)
admin.site.register(MenuItem)