from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=10, validators = [RegexValidator(regex=r'^\d{10}$', message="Phone Number must contain 10 digits.", code="Invalid Phone Number", inverse_match=None, flags=0)], blank=True, unique=True)
    is_email_verify = models.BooleanField(default=False)
    latitude = models.FloatField(blank=True)
    longitude = models.FloatField(blank=True)
    
    def __str__(self):
        return self.user.username

class Restaurant(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=10, validators = [RegexValidator(regex=r'^\d{10}$', message="Phone Number must contain 10 digits.", code="Invalid Phone Number", inverse_match=None, flags=0)], blank=True, unique=True)
    is_email_verify = models.BooleanField(default=False)
    latitude = models.FloatField(blank=True)
    longitude = models.FloatField(blank=True)
    postcode = models.CharField(max_length=15,blank=True)
    city = models.CharField(max_length=15,blank=True)
    address = models.CharField(max_length=200,blank=True)

    def __str__(self):
        return self.name

class Order(models.Model):
    restaurant = models.OneToOneField(Restaurant, on_delete=models.PROTECT)
    customer = models.OneToOneField(Customer, on_delete=models.PROTECT)
    order_total = models.FloatField()
    
    def __str__(self):
        return str(self.id)

class Menu(models.Model):
    restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name

class SubMenu(models.Model):
    menus = models.ForeignKey(Menu, on_delete=models.CASCADE, related_name='submenus')
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200, blank=True)
    
    def __str__(self):
        return self.name

class MenuItem(models.Model):
    submenu = models.ForeignKey(SubMenu, on_delete=models.CASCADE, related_name='submenu_items')
    order = models.ForeignKey(Order, on_delete=models.PROTECT, null=True, blank=True, related_name='ordered_items')
    name = models.CharField(max_length=50)
    price = models.FloatField()
    description = models.CharField(max_length=200, blank=True)
    
    def __str__(self):
        return self.name
