from rest_framework import serializers
from api.models import Customer, Restaurant, Menu, SubMenu, Order, MenuItem
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

"""
Serializers
"""

class UserSerializer(serializers.ModelSerializer):

	class Meta:
		model = User
		fields = '__all__'

	def create(self, validated_data):
		validated_data['password'] = make_password(validated_data['password'])
		return super(UserSerializer, self).create(validated_data)

	def update(self, instance, validated_data):
		validated_data['password'] = make_password(validated_data['password'])
		return super(UserSerializer, self).update(instance, validated_data)

class CustomerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Customer
		fields = '__all__'
		depth = 2

class RestaurantSerializer(serializers.ModelSerializer):
	class Meta:
		model = Restaurant
		fields = '__all__'
		depth = 2

class MenuSerializer(serializers.ModelSerializer):
	class Meta:
		model = Menu
		fields = '__all__'
		depth = 2

class SubMenuSerializer(serializers.ModelSerializer):
	class Meta:
		model = SubMenu
		fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
	class Meta:
		model = Order
		fields = '__all__'

class MenuItemSerializer(serializers.ModelSerializer):
	class Meta:
		model = MenuItem
		fields = '__all__'
