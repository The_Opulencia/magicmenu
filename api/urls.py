from rest_framework import routers
from .api import CustomerViewSet, RestaurantViewSet, UserViewSet , MenuViewSet, SubMenuViewSet, MenuItemViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register('user', UserViewSet, 'user')
router.register('customer', CustomerViewSet, 'customer')
router.register('restaurant', RestaurantViewSet, 'restaurant')
router.register('menu', MenuViewSet, 'menu')
router.register('submenu', SubMenuViewSet, 'submenu')
router.register('menuitem', MenuItemViewSet, 'menuitem')
router.register('order', OrderViewSet, 'order')

urlpatterns = router.urls