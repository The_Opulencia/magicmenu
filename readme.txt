 API Documentation
===================

	BASE_URL = domain_name
	like 'http://127.0.0.1:8000' 

a) Authentication:
   =================
   1. Command to Generate token_key : python manage.py drf_create_token username

b) User:
   ======
   1. extract all users : 
   		url = 'BASE_URL/api/user/'
   		request_type = 'GET'
   		Header :
   			Authorization : Token token_key

   2. extract specfic user :
   		url = 'BASE_URL/api/user/user_id/'
   		request_type = 'GET'
   		Header :
   			Authorization : Token token_key

   3. delete specfic user :
   		url = 'BASE_URL/api/user/user_id/'
   		request_type = 'DELETE'
   		Header :
   			Authorization : Token token_key

   4. register USER :
   		url = 'BASE_URL/api/user/'
   		request_type = 'POST'
   		Header :
   			Authorization : Token token_key
   			Content-Type : application/json
   		Body :
   			json object

   5. Update user :
   		url = 'BASE_URL/api/user/user_id/'
   		request_type = 'PUT'
   		Header :
   			Authorization : Token token_key
   			Content-Type : application/json
   		Body :
   			json object

   	Database
   	========

   	1. Json Object
   		{
        "id": 11,
        "password": "pbkdf2_sha256$180000$Mz0BC79qT2ks$olvJNE1znx5TpKjwDZ68kI85gsZ8fY22gAlxrOsFbsw=", 
        "last_login": null,
        "is_superuser": false,
        "username": "tiaclairtoomey",
        "first_name": "Tia",
        "last_name": "Clair tommey",
        "email": "tia@gmail.com",
        "is_staff": false,
        "is_active": true,
        "date_joined": "2020-07-13T08:42:35.643232Z",
        "groups": [],
        "user_permissions": []
    }

    Imp. 
    	Django built in User Model is used.
    	username field is not case sensitive so check in database before adding.
    	email field is not unique so check in database before adding.
    	check this out for more info : https://docs.djangoproject.com/en/3.0/ref/contrib/auth/

c) Customer:
   ==========

  	url = 'BASE_URL/api/customer/'

  	works same as User

  	1. register Customer :
   		url = 'BASE_URL/api/customer/'
   		request_type = 'POST'
   		Header :
   			Authorization : Token token_key
   			Content-Type : application/json
   		Body :
   			json object where user field contains user_id

  	Database:
  	=========
  	1. Json Object

  		{
        "id": 1,
        "phone_number": "9191919191",
        "is_email_verify": true,
        "latitude": 10.354,
        "longitude": 20.398,
        "user": {
            "id": 2,
            "password": "pbkdf2_sha256$180000$Mz0BC79qT2ks$olvJNE1znx5TpKjwDZ68kI85gsZ8fY22gAlxrOsFbsw=",
            "last_login": null,
            "is_superuser": false,
            "username": "ritvik",
            "first_name": "Ritvik",
            "last_name": "Harsh",
            "email": "ritvik@gmail.com",
            "is_staff": false,
            "is_active": true,
            "date_joined": "2020-07-12T18:13:13Z",
            "groups": [],
            "user_permissions": []
        }
    }

d) Restaurant:
   ============
	
	url = 'BASE_URL/api/restaurant/'

	works same as Customer

	Database:
  	=========

  	Imp. : user is a owner of restaurant
  	
  	1. Json Object:

  	{
        "id": 1,
        "name": "Crossroads",
        "phone_number": "1234567890",
        "is_email_verify": false,
        "latitude": 12.0,
        "longitude": 11.0,
        "postcode": "323030",
        "city": "Jdr",
        "address": "31",
        "user": {
            "id": 2,
            "password": "pbkdf2_sha256$180000$Mz0BC79qT2ks$olvJNE1znx5TpKjwDZ68kI85gsZ8fY22gAlxrOsFbsw=",
            "last_login": null,
            "is_superuser": false,
            "username": "ritvik",
            "first_name": "Ritvik",
            "last_name": "Harsh",
            "email": "ritvik@gmail.com",
            "is_staff": false,
            "is_active": true,
            "date_joined": "2020-07-12T18:13:13Z",
            "groups": [],
            "user_permissions": []
        }
    }

e) Menu
  =======

  url = 'BASE_URL/api/menu/'

  Database:
  =========

  1. Extract All Restaturant Menu

    url = 'BASE_URL/api/menu/'
    request_type = 'GET'
    Header :
      Authorization : Token token_key
      Content-Type : application/json

  2. Specfic Menu

    url = 'BASE_URL/api/menu/restaurant_id/'
    request_type = 'GET'
    Header :
      Authorization : Token token_key
      Content-Type : application/json

  3. Add and Update

    url = 'BASE_URL/api/menu/menu_id/' for PUT
    url = 'BASE_URL/api/menu/' for POST

    Header :
      Authorization : Token token_key
      Content-Type : application/json
    Body :
      Json Object

  5. Json Object
    {
        "id": 1,
        "name": "OTR Menu",
        "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,",
        "restaurant": {
            "id": 2,
            "name": "On The Rocks",
            "phone_number": "0123456789",
            "is_email_verify": true,
            "latitude": 1.0,
            "longitude": 12.0,
            "postcode": "123421",
            "city": "Jdr",
            "address": "Jdr",
            "user": {
                "id": 5,
                "password": "pbkdf2_sha256$180000$Fd5JtAOdMlz7$SxwkH5kP8l1z2dKERE+hbXF+RptmN88HczG+7nZdOI8=",
                "last_login": null,
                "is_superuser": false,
                "username": "Vikramvaibhav",
                "first_name": "",
                "last_name": "",
                "email": "",
                "is_staff": false,
                "is_active": true,
                "date_joined": "2020-07-12T18:40:32.343116Z",
                "groups": [],
                "user_permissions": []
            }
        }

f) SubMenu:
   ========

   url = 'BASE_URL/api/submenu/'

   works same as Menu

   DATABASE
   ========

   1. Json Object:
   {
        "id": 1,
        "name": "Mojito",
        "description": "Mojito is a traditional Cuban highball",
        "menus": 1
    }

  Imp. : "menus" represents Menu object id

g) MenuItem:
  ==========

  url = 'BASE_URL/api/menuitem/'

  DATABASE
  ========

  1. Json Object
    {
        "id": 2,
        "name": "Tacos",
        "price": 260.0,
        "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,",
        "submenu": 2,
        "order": 1
    }

    Imp. : "submenus" represents SubMenu object i
           "order" represents Order object id

h) Order:
  ======

  url = 'BASE_URL/api/order/'

  DATABASE
  ========

  1. Json Object

      {
        "id": 1,
        "order_total": 260.0,
        "restaurant": 2,
        "customer": 3
      }